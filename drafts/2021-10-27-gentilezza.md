---
published: false
layout: post
title: "Gentiluce"
date: 2021-10-21
description: 
image: /assets/images/musei/ingresso_liberato.jpg
author: Marco.Djallo
tags: 
  - Colori
  - Natura
---
![Placeholder](/assets/images/skate.jpg)

Tante volte la luce accarezza in maniera così gentile  
non metalli nobili   
ma superfici per noi così ordinarie, basse, volgari;  
forse  
abbiamo più ricchezze di quelle che pensiamo.