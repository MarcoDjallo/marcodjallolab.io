---
published: true
layout: post
title: "RadicarSé"
date: 2022-01-23
description: 
image: /assets/images/thumbs/radicarse_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
---
![Placeholder](/assets/images/radicarse.jpg)

Pensavo solo...  
Pensavo solo.  
Pensavo, solo.  
E quanto.  
<br>
Ma non ci penso più tanto.  
E non sono più solo,  
da quando ci sono io.


