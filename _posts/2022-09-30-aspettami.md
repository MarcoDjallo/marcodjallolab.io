---
published: true
layout: post
title: "Aspettami"
date: 2022-09-30
description: 
image: /assets/images/thumbs/aspettami_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/aspettami.jpg)

Come quella paura che si ha da bambinз  
di esser lasciatз indietro  
da solз.  
<br>
Che poi,  
è la stessa che abbiamo da adultз.