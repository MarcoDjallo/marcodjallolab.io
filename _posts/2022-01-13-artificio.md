---
published: true
layout: post
title: "Folli d'artificio"
date: 2022-01-13
description: 
image: /assets/images/thumbs/artificio_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Bianco e Nero
  - Natura
---

![Placeholder](/assets/images/artificio.jpg)

Ricreiamo continuamente  
brutte copie di ciò che già abbiamo  
produciamo e non curiamo  
tagliamo e non guardiamo  
buttiamo e non ricordiamo  
per poi consumarci e perdere copie e originali  
per poi consumarci e perdere entrambə  
per poi consumarci  
per poi perdere.  