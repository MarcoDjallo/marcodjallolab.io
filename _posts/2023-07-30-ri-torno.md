---
layout: post
published: "true"
title: Ri.Torno.
date: 2023-07-30T08:37:15.831Z
image: /assets/images/thumbs/torno_1_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - RiEmergere_Vacanze22
  - colori
---
Torno solo,  
ma torno solo se  
se poi ti ritrovo.

![](/assets/images/torno_1.jpg)

![](/assets/images/torno_2.jpg)

![](/assets/images/torno_3.jpg)