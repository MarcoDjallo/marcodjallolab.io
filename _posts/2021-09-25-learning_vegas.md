---
published: true
layout: post
title: "Learning (badly) from Las Vegas"
date: 2021-09-25
description: 
image: /assets/images/thumbs/vegas.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Urban
  - Buio
---

![Placeholder](/assets/images/vegas.jpg)

Amo la notte per le strade delle cittadine di periferia. Tuttə dormono, nulla accade, e puoi stare per davvero insieme ai tuoi pensieri. Come se il mondo fosse solo tuo, durante quelle ore di oscurità.  

Questo scatto è il primo esperimento che faccio con le simulazioni di pellicola in post produzione. Applicato c'è un profilo pellicola Kodak Elite Color 400.