---
published: true
layout: post
title: "Æquino(i)ctium"
date: 2023-03-05
description: 
image: /assets/images/thumbs/aequinoictium_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/aequinoictium.jpg)

La luce colpisce tuttз allo stesso modo  
proietta tuttз sullo stesso piano  
umani e umani  
umani e natura  
natura e natura  
umani è natura  
<br>
Forse ha ragione lei.