---
published: false
layout: post
title: "Stiamo tornando; anzi, non ce ne siamo mai andati"
date: 2021-09-24
description: 
image: /assets/images/thumbs/bici.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Urban
  - Bici
  - Politica
---

![Placeholder](/assets/images/bici.jpg)

In bici o con qualsiasi altro mezzo, noi comunisti stiamo tornando.  

Più affamati che mai di mezzi di produzione, veniamo dal basso  
come un gancio al mento  
mentre tu  
ridi.
