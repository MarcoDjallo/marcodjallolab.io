---
published: true
layout: post
title: "Quella volta nostra"
date: 2021-11-20
description: 
image: /assets/images/thumbs/quella_volta.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Persone
---

![Placeholder](/assets/images/quella_volta.jpg)

Stai sempre a tirar fuori  
le stesse cose  
quella volta che ci siamo incontrati  
quella volta che ci amavamo  
quella volta che non era certo la vita che avrei voluto  
quella volta che ci siamo insultati senza fine  
quella volta che è passato tutto  
quella volta che avrei fatto tutt'altre scelte  
quella volta che ci hanno costretti insieme  
quella volta che era tutto ciò che desideravo  
quella volta che ci odiavamo  
quella volta che non avevo rimorsi  
quella volta che non cambierei nulla  
quella volta che eri propri tu e solo tu  
quella volta che ci amiamo ancora  
quella volta che ci siamo accontentati in silenzio  
quella volta che abbiamo fatto tutto giusto  
quella volta che ci odiamo  
quella volta che è tornato tutto  
quella volta che che cazzo ne sa un ragazzo alle nostre spalle con la sua fotocamera
che la vita è un casino  
e che ognunə ha il suo  
e che quella volta che abbiamo deciso che in fondo
potevamo mettere i nostri casini in fila
è stata un casino lunghissimo  
ma è stata quella volta che era la nostra vita.

