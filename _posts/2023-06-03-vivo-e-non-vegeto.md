---
layout: post
published: "true"
title: Vivo (e non vegeto)
date: 2023-06-03T10:04:28.567Z
image: /assets/images/thumbs/vivo_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - RiEmergere_Vacanze22
  - colori
---
![](/assets/images/vivo.jpg)

Finché mi capiterà di stupirmi anche soltanto di fronte alla finestra di un bagno  
saprò di essere ancora in vita.