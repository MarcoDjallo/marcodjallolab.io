---
published: true
layout: post
title: "Scegliere"
date: 2023-02-20
description: 
image: /assets/images/thumbs/scegliere_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Città
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/scegliere.jpg)

Potevi dirmi dove andare  
quantз l'hanno fatto.  
In buona e cattiva fede, si intende;  
ma tu no.  
<br>
Eppure,  
ci sono posti dove mi porti solo tu.  
Portamici per sempre  
e non ci pensiamo più.  

