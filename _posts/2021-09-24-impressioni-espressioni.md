---
published: true
layout: post
title: "Impressioni | Espressioni"
date: 2021-09-24
description: 
image: /assets/images/thumbs/inmezzo_1.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Urban
  - Natura
  - Vuoto
---

![Placeholder](/assets/images/pellerina/inmezzo_1.jpg)

<blockquote>
  <p>“Una foto è l'espressione di un'impressione. Se il bello non era in noi, come potremmo mai riconoscerlo?”</p>
  <cite>Ernst Haas</cite>
</blockquote>

Ci sono scatti che faccio, ma non so perché. Alle volte vedo cose che, anche nella loro banalità, riescono a superare le mie barriere razionali e interpellano direttamente la mia parte più profonda, chiedendo di essere impresse, fermate dentro la cornice del mirino.  

Così, senza aver chiaro il perché, compongo e scatto. Con la stessa attitudine, come direbbe qualcuno *"inseguendo una sensazione più che un'idea sensata"*, sviluppo, ritaglio, ed esporto.  

E facendo questo, alle volte trovo dei sensi, o anche se non li trovo, rifletto. Un processo meditativo, direi.

![Placeholder](/assets/images/pellerina/inmezzo_2.jpg)

Sicuramente, ho capito di essere sensibile ad alcune situazioni.  
Gli squarci di interventi umani lasciati nel mondo che in qualche modo vengono lasciati a loro stessi, come dimenticati, ormai diventati elementi quasi ancestrali, quasi *naturali*.  
E poi alcune geometrie e contrasti, proprio tra gli interventi umani e la natura vera e propria.  
Qualcosa del genere. Qualcosa che approfondirò ulteriormente.

![Placeholder](/assets/images/pellerina/inmezzo_3.jpg)