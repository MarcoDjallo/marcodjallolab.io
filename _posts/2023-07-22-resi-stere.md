---
layout: post
published: "true"
title: Resi(stere)
date: 2023-07-22T08:36:50.230Z
image: /assets/images/thumbs/resistere_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - RiEmergere_Vacanze22
  - colori
  - viste
---
![](/assets/images/resistere.jpg)

Ti lasci andare  
cadi  
ti accasci, su quel che c'è  
o che non c'è  
ma per quanto vai giù  
un corpo galleggia sempre  
e da lì puoi riprendere  
a nuotare.