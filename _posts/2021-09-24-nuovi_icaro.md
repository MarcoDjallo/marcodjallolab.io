---
published: true
layout: post
title: "Nuovi Icaro"
date: 2021-09-24
description: 
image: /assets/images/thumbs/skate.jpg
author: Marco.Djallo
categories: post
tags: 
  - Bianco e Nero
  - Urban
  - Sport
---

![Placeholder](/assets/images/skate.jpg)

Uno dei primi scatti con la mia nuova (o meglio, ricondizionata :D ) Panasonic GX-80 con lente 20mm f1.7.  

La sto amando. Piccola, relativamente leggera, tutta nera. Il 20mm equivale a 40mm su full frame, circa il campo visivo umano. 

Mi posiziono, inclino il display, e senza troppo clamore scatto.  