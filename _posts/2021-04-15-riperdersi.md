---
published: true
layout: post
title: "Riperdersi"
date: 2022-04-15
description: 
image: /assets/images/thumbs/riperdersi_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
---
![Placeholder](/assets/images/riperdersi.jpg)

Ogni volta  
che restringi i bordi  
lasci fuori il rumore  
intorno a ciò che guardi  
scopri un nuovo, intero, mondo.  
E poi di nuovo.  
<br>
Lascia perderti,  
tanto, che hai di meglio
da fare.  
Mal che vada,    
ti ritrovi.
