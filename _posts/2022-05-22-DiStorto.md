---
published: true
layout: post
title: "DiStorto, diretto"
date: 2022-05-22
description: 
image: /assets/images/thumbs/DiStorto_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/DiStorto.jpg)

E se le imperfezioni non fossero nello specchio  
ma nel soggetto che esso riflette?  
Saremmo salvз,  
saremmo umanз  
di nuovo.