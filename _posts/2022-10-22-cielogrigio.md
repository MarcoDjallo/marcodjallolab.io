---
published: true
layout: post
title: "#01//Un cielo grigio"
date: 2022-10-22
description: 
image: /assets/images/thumbs/grigio01_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/cielogrigio/grigio01.jpg)

Il cielo era grigio  
l'aria umida  
e da qualche parte c'era un motivo per fermarsi.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio02.jpg)

Il cielo era grigio  
l'aria umida  
l'orizzonte inesistente.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio03.jpg)

Il cielo era grigio  
l'aria umida  
e questi i soli abitanti.  

<br>
<br>