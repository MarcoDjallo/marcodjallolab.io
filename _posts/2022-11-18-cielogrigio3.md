---
published: true
layout: post
title: "#03//Un cielo grigio"
date: 2022-11-18
description: 
image: /assets/images/thumbs/grigio07_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/cielogrigio/grigio07.jpg)

Il cielo era grigio  
l'aria umida  
e ciò che vedevo sembrava una fotografia.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio08.jpg)

Il cielo era grigio  
l'aria umida  
e quella fotografia sapeva di un futuro  
grigio.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio09.jpg)

Il cielo era grigio  
l'aria umida  
ma alla fine,  
che importanza aveva?  

<br>
<br>