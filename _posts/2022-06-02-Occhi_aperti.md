---
published: true
layout: post
title: "Occhi aperti"
date: 2022-06-02
description: 
image: /assets/images/thumbs/occhi_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
---
![Placeholder](/assets/images/occhi.jpg)

E se apparissi qui  
davanti ai miei occhi  
fuori da ogni logica  
senso  
probabilità  
piano  
aspettativa?  
<br><br>
In effetti,
è successo.