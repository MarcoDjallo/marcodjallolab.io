---
published: true
layout: post
title: "#02//Un cielo grigio"
date: 2022-11-01
description: 
image: /assets/images/thumbs/grigio04_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/cielogrigio/grigio04.jpg)

Il cielo era grigio  
l'aria umida  
e qualcunə provava a distinguersi.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio05.jpg)

Il cielo era grigio  
l'aria umida  
e l'essere umano c'era solo nelle sue geometrie.  

<br>
<br>

![Placeholder](/assets/images/cielogrigio/grigio06.jpg)

Il cielo era grigio  
l'aria umida  
e ho visto qualcosa.  

<br>
<br>