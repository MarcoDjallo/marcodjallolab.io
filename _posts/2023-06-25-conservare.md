---
layout: post
published: "true"
title: Conservare
date: 2023-06-25T17:21:58.902Z
image: /assets/images/thumbs/conservare_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - RiEmergere_Vacanze22
  - colori
---
![](/assets/images/conservare.jpg)

Spesso scatto solo per conservare la luce che vedo

perché spesso è bellissima.