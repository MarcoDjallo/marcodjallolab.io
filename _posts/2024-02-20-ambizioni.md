---
layout: post
published: "true"
title: Ambizioni
date: 2024-02-20T20:25:36.636Z
image: /assets/images/thumbs/ambizioni_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - Colori
  - Natura
---
Alcuni scatti\
sono lo specchio\
di come vorrei\
i miei pensieri.  



![](/assets/images/ambizioni.jpg)