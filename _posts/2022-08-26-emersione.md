---
published: true
layout: post
title: "Emersione"
date: 2022-08-26
description: 
image: /assets/images/thumbs/emersione_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - Acqua
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/emersione.jpg)

Dove vai tu  
e io dove vado  
ma  
non lo so.  
Che poi alla fine  
cerchiamo tuttз una boccata d'aria e una lama di luce  
tra una bracciata e l'altra in quest'acqua pesante.
<br>
<br>
E ci sono occhi in cui le trovi.