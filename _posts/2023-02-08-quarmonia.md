---
published: true
layout: post
title: "QuArmonia"
date: 2023-02-08
description: 
image: /assets/images/thumbs/quarmonia_pre.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - RiEmergere_Vacanze22
---
![Placeholder](/assets/images/quarmonia.jpg)

Perché ogni vivente  
sa esattamente dove mettersi.  
<br>
Solo gli umani  
non sanno stare al loro posto.  