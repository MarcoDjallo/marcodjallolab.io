---
layout: post
published: "true"
title: La Confluenza
date: 2023-07-29T10:02:41.253Z
image: /assets/images/thumbs/confluenza1_pre.jpg
author: Marco.Djallo
categories: post
tags:
  - RiEmergere_Vacanze22
  - colori
---
![](/assets/images/confluenza1.jpg)

Ho camminato, ho camminato. Finché, ho raggiunto un luogo: la confluenza.\
Due fiumi si incontrano, le acque di colore diverso si mescolano in una soluzione omogenea e inscindibile. Di certo, non é il posto più selvaggio in cui sia mai stato.   

Eppure. Eppure c'era una risonanza. Era un mio posto.   

Esistono posti dove la magnificenza, il comfort di quanto vediamo, sentiamo, viviamo ci fa sentire unitз in noi stessз ? Posti che risuonano con noi, come se un pezzo del nostro essere fosse stato nascosto lì alla nostra nascita, in attesa di essere ritrovato, per poterci sentire una frazione di meno incompleti?   

Io, credo di sì. Questo é quanto ho sentito. La confluenza.\
Corpo e mente, fluiscono nello stesso.\
In una soluzione omogenea e inscindibile, che sono io.   

![](/assets/images/confluenza2_1.jpg)

![](/assets/images/confluenza3.jpg)