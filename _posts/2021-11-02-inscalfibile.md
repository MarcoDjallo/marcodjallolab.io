---
published: true
layout: post
title: "Inscalfibile"
date: 2021-11-02
description: 
image: /assets/images/thumbs/inscalfibile.jpg
author: Marco.Djallo
categories: post
tags: 
  - Bianco e Nero
  - Natura
  - Persone
---

![Placeholder](/assets/images/inscalfibile.jpg)

Sere passate a lanciare sassi al futuro  
gli stessi che lanciavamo in passato  
nel presente.  

E siamo come loro  
a mezz'aria fino all'impatto  
tuoniamo sul terreno  
qualche graffio  
poi silenzio,  
la vera fiducia non se n'è mai fatta nulla delle parole.  
Imperterriti,  
fino al prossimo volo.  

E anche quando atterriamo distanti  
siamo la stessa pietra  
salda e fiera come la tua amicizia.

