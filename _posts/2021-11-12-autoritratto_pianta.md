---
published: true
layout: post
title: "Autoritratto con fotoamatore e pianta (a mirrorless in the mirror)."
date: 2021-11-12
description: 
image: /assets/images/thumbs/autopianta.jpg
author: Marco.Djallo
categories: post
tags: 
  - Colori
  - Natura
  - Persone
  - Autoritratto
---

I miei occhi nel tuo, il tuo occhio nei miei  
ormai quando guardo, tu sempre ci sei.

![Placeholder](/assets/images/autopianta.jpg)

<blockquote>
  <p>“La macchina fotografica è uno strumento che insegna alle persone come vedere senza macchina fotografica.”</p>
  <cite>Dorothea Lange</cite>
</blockquote>